# Feature Importance

This repository is an exploration of different methods to measure (global) feature importance in ensemble of trees (random forests, extra trees, gradient boosting).

## Environment

To install the environment, run

```
conda env create -f environment.yml
```

After the installation is complete, activate the environment with

```
conda activate featureimportance
```