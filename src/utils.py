import pandas as pd
from pandas.api.types import CategoricalDtype
from typing import Union


def replace_yesno_with_cat(df: pd.DataFrame) -> pd.DataFrame:
    """
    Replace Yes/No columns with categorical variables
    """
    
    for col in df:
        if set(df[col].unique()) == {'No', 'Yes'}:
            df[col] = df[col].astype(CategoricalDtype(categories=['No','Yes'], ordered=True))
        elif set(df[col].unique()) == {0,1}:
            df[col] = df[col].astype(CategoricalDtype(categories=[0,1], ordered=True))
            df[col] = df[col].cat.rename_categories({0: "No", 1: "Yes"})
    return df


def replace_object_with_cat(df: pd.DataFrame) -> pd.DataFrame:
    for col in df.select_dtypes(include='object'):
        df[col] = df[col].astype('category')
    return df


def load_telco_data(file: str) -> pd.DataFrame:
    df = pd.read_csv(file, na_values=' ').drop(columns = 'customerID')
    df = replace_yesno_with_cat(df)
    df = replace_object_with_cat(df)
    
    return df